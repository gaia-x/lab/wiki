# Gaia-X Lab

The Gaia-X Lab is a development team under the Gaia-X CTO office.

## Missions

The missions of the Gaia-X Lab are to:

- write prototypes to technically validate functional and technical hypothesis made by the Gaia-X Working Groups, when working on the Gaia-X specifications.
- demonstrate working prototypes.
- accelerate the development of external OpenSource Software (OSS) project.
- identify missing functional components in the Gaia-X Specifications

## Deliverables

- The results of the prototyping activities are shared with the Gaia-X Working Groups and the Gaia-X Lighthouses projects for their own considerations.
- The Gaia-X Lab doesn’t contribute to the Gaia-X specs.
- The Gaia-X Lab doesn’t release any official Gaia-X deliverables.
- All working activities are publicly done in the [current repository](https://gitlab.com/gaia-x/lab/).

Our backlog is available here <https://gaia-x.atlassian.net/browse/LAB>.

## FAQ

**What happens to the prototyping code ?**

The result of the prototyping work is always shared with the Gaia-X Working Groups.


- If the prototype is unsuccessful, the git repository is archived.
- If the prototype is successful: 

| Usecase     | Action |
|-------------|--------|
| **Accelerator** = the prototype is a feature of an existing OSS project. | It's contributed back to the OSS project. |
| **Filler** = the prototype identified a missing Gaia-X Component. | It's released as a new OSS project under the Eclipse Foundation.[^gxgov] |
| **Demo** = the prototype showcases a workflow for educational purpose | It's released as a new OSS project under the Gaia-X OSS rules. |

[^gxgov]: _The exceptions are the Gaia-X Registry and the Gaia-X Compliance services that will be released as OSS projects under the Gaia-X OSS rules._

**What licenses are used ?**

All Gaia-X Lab projects follow the Gaia-X OSS rules. The license is either **Apache-2.0** or **EPL-2.0**.

**How can I contact the Gaia-X Lab ?**

The preferred contact point with the Gaia-X Lab team is the [current repository](https://gitlab.com/gaia-x/lab/). Please open ticket and merge-request for questions and improvements.

**Can and How can I contribute ?**

Everyone, member and non-member are welcomed to send merge-request for review by the Gaia-X Lab team.

**How can I join the Gaia-X Lab team ?**

Please check the Gaia-X website for open-positions <https://www.gaia-x.eu/open-jobs>.[^join]

[^join]: _The Gaia-X Lab is not a Gaia-X Working Group._

You can also directly contribute via this repository by opening tickets and submitting merge-requests.

---

Happy Coding !

Gaia-X Lab team

---

