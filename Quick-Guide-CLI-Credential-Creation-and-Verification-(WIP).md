# Quick Guide CLI Credential Creation and Verification

We use this quick guide to allow reproduction

* Creating a DID:WEB for the issuer
* Creating a DID:WEB for the participant company
* Creating a first participant company Verifiable Credential (VC)
* Verification of the first participant company VC
* Creating a first participant company Verifiable Presentation (VP)
* Verification of the first participant company VP

Additionally we create a bridge to the work of the Sub-WG Self-Descriptions and make use of the SHACL Shapes and the SD Wizard to create a first Self-Description JSON file (unsigned) and to validate it against the reference shape.

This also helps to track open issues that emerge from this test and further plan the roadmap.

## Goals

* Enable the creation of a Verifiable Credential according to the Gaia-X Trust Framework (WIP). <https://gaia-x.gitlab.io/policy-rules-committee/trust-framework/>
* Provide a demonstrator to evaluate the current state of specifications and accelerate open-source development.
* Contribute back to the open-source software projects and increase the sync between participants working on the topic.
* ...

# Walkthrough

## Creating a basic SHACL Shape

The shapes are prepared in line of the Self-Description Working Group which you can follow here: <https://gitlab.com/gaia-x/gaia-x-community/gaia-x-self-descriptions/-/tree/master/implementation/validation>

Example Shape

```plaintext
@prefix sh:                 <http://www.w3.org/ns/shacl#> .
@prefix gax-core:           <http://w3id.org/gaia-x/core#> .
@prefix gax-participant:    <http://w3id.org/gaia-x/participant#> .
@prefix vcard:              <http://www.w3.org/2006/vcard/ns#> .
@prefix xsd:                <http://www.w3.org/2001/XMLSchema#> .
@prefix :                   <http://w3id.org/gaia-x/validation#> .

:Participant
    a              sh:NodeShape ;
    sh:targetClass gax-participant:participant ;
    sh:property     [ sh:path gax-participant:hasRegistrationNumber ;
                      sh:order 1 ;
                      sh:name "registration number" ;
                      sh:description "legal registration number of an organisation." ;
                      sh:minCount 1 ;
                      sh:maxCount 1 ;
                      sh:datatype xsd:string ] ;
    sh:property     [ sh:path gax-participant:hasLegallyBindingName ;
                      sh:order 2 ;
                      sh:name "legally binding name" ;
                      sh:minCount 1 ;
                      sh:maxCount 1 ;
                      sh:datatype xsd:string ] ;
    sh:property     [ sh:path gax-participant:hasJurisdiction ;
                      sh:order 3 ;
                      sh:name "jurisdiction under which an organization is incorporated." ;
                      sh:minCount 1 ;
                      sh:maxCount 1 ;
                      sh:datatype xsd:string ] ;
    sh:property     [ sh:path gax-participant:hasCountry ;
                      sh:order 4 ;
                      sh:name "country the HQ is based in." ;
                      sh:minCount 1 ;
                      sh:maxCount 1 ;
                      sh:datatype xsd:string ] ;
    sh:property     [ sh:path gax-participant:leiCode ;
                      sh:order 5 ;
                      sh:name "unique LEI number" ;
                      sh:minCount 0 ;
                      sh:maxCount 1 ;
                      sh:datatype xsd:string ] ;
    sh:property     [ sh:path gax-participant:ethereumAddress ;
                      sh:order 6 ;
                      sh:name "ethereum address" ;
                      sh:minCount 0 ;
                      sh:maxCount 1 ;
                      sh:datatype xsd:string ] ;
    sh:property     [ sh:path gax-participant:parentOrganisation ;
                      sh:order 7 ;
                      sh:name "a list of participants that this entity is a sub-organization of" ;
                      sh:minCount 0 ;
                      sh:datatype xsd:string ] ;
    sh:property     [ sh:path gax-participant:subOrganisation ;
                      sh:order 8 ;
                      sh:name "a list of participants with an legal mandate on this entity, e.g., as a subsidiary" ;
                      sh:minCount 0 ;
                      sh:datatype xsd:string ] ;
.
```

## Creating a basic Self-Description as JSON

The Self-Description can be generated using the TTL file containing the SHACL shape and the SD Wizard / SHACL form generator tool <https://gaia-x.fit.fraunhofer.de/>

The result might look like this:

```plaintext
{
  "@context": {
    "0": "http://w3id.org/gaia-x/validation#",
    "gax-participant": "http://w3id.org/gaia-x/participant#",
    "sh": "http://www.w3.org/ns/shacl#",
    "xsd": "http://www.w3.org/2001/XMLSchema#",
    "vcard": "http://www.w3.org/2006/vcard/ns#",
    "gax-core": "http://w3id.org/gaia-x/core#"
  },
  "@id": "http://example.org/participant-dp6gtq7i75lmk9p4j2tfg",
  "@type": "gax-participant:participant",
  "gax-participant:hasRegistrationNumber": {
    "@value": "DEK1101R.HRB170364",
    "@type": "xsd:string"
  },
  "gax-participant:hasLegallyBindingName": {
    "@value": "deltaDAO AG",
    "@type": "xsd:string"
  },
  "gax-participant:hasJurisdiction": {
    "@value": "GER",
    "@type": "xsd:string"
  },
  "gax-participant:hasCountry": {
    "@value": "GER",
    "@type": "xsd:string"
  },
  "gax-participant:leiCode": {
    "@value": "391200FJBNU0YW987L26",
    "@type": "xsd:string"
  },
  "gax-participant:ethereumAddress": {
    "@value": "0x4C84a36fCDb7Bc750294A7f3B5ad5CA8F74C4A52",
    "@type": "xsd:string"
  }
}
```

## Validation of a basic Self-Description against the Shape

To check the created Self-Description against the SHACL shape you can use a tool like the SHACL playground. <https://shacl-playground.zazuko.com/>

## Bridging SD-Shapes to VC-Shapes

work in progress.

The currently used shapes can be located in the VC Lib here: <https://github.com/walt-id/waltid-ssikit-vclib/tree/master/src/main/kotlin/id/walt/vclib/credentials/gaiax>

The JSON Schema used is located here: <https://raw.githubusercontent.com/walt-id/waltid-ssikit-vclib/master/src/test/resources/schemas/ParticipantCredential.json>

The test JSON schemas are located here: <https://github.com/walt-id/waltid-ssikit-vclib/tree/master/src/test/resources/schemas>

These need to be closed aligned with the work of the SD group and need to include the validation logic later on.

## Creating DID:WEB for Issuer and Participant Company

For the follow steps we use the open-source SSI Kit provided by walt.id under Apache 2.0 License. <https://github.com/orgs/walt-id/repositories?type=all>, the documentation can be found here <https://walt-id.gitbook.io/walt.id/v/web-wallet/what-is-the-wallet/readme>.

To use the CLI tool to follow this tutorial you will need docker. <https://www.docker.com/products/docker-desktop/>

Create the following local folder.

`mkdir did-service && cd did-service`

Pull the latest docker image.

`docker pull docker.io/waltid/ssikit`

Set an alias.

`alias ssikit="docker container run -p 7001-7005:7001-7005 -itv $(pwd)/data:/app/data docker.io/waltid/ssikit"`

Create new keypairs or import existing ones.

```plaintext
ssikit key gen --algorithm Ed25519

ssikit key gen --algorithm RSA

ssikit key list
```

Create the DIDs using your preferred domains and host them in the well-known folder. Use the IDs of your keypairs (other than the example below).

```plaintext
ssikit did create -m web -d vc.lab.gaia-x.eu -k 8eb4a104005948c886300d40daf7925f

Install this did:web at:  https://vc.lab.gaia-x.eu/.well-known//did.json

ssikit did create -m web -d delta-dao.com -k 93cfc125cafc43f5acdbef26e5911d00

Install this did:web at: https://delta-dao.com/.well-known//did.json
```

Example for Issuer <https://vc.lab.gaia-x.eu/.well-known/did.json>

```plaintext
{
    "assertionMethod" : [
        "did:web:vc.lab.gaia-x.eu#8eb4a104005948c886300d40daf7925f"
    ],
    "authentication" : [
        "did:web:vc.lab.gaia-x.eu#8eb4a104005948c886300d40daf7925f"
    ],
    "@context" : "https://www.w3.org/ns/did/v1",
    "id" : "did:web:vc.lab.gaia-x.eu",
    "verificationMethod" : [
        {
            "controller" : "did:web:vc.lab.gaia-x.eu",
            "id" : "did:web:vc.lab.gaia-x.eu#8eb4a104005948c886300d40daf7925f",
            "publicKeyJwk" : {
                "alg" : "EdDSA",
                "crv" : "Ed25519",
                "kid" : "8eb4a104005948c886300d40daf7925f",
                "kty" : "OKP",
                "use" : "sig",
                "x" : "VyReBUUYvcC7jGCOwQHMHRPad4ol5BUBUFKIf5pUeBs"
            },
            "type" : "Ed25519VerificationKey2019"
        }
    ]
}
```

Example for Participant Company <https://www.delta-dao.com/.well-known/did.json>

```plaintext
{
  "assertionMethod": [
    "did:web:delta-dao.com#93cfc125cafc43f5acdbef26e5911d00"
  ],
  "authentication": [
    "did:web:delta-dao.com#93cfc125cafc43f5acdbef26e5911d00"
  ],
  "@context": "https://www.w3.org/ns/did/v1",
  "id": "did:web:delta-dao.com",
  "verificationMethod": [
    {
      "controller": "did:web:delta-dao.com",
      "id": "did:web:delta-dao.com#93cfc125cafc43f5acdbef26e5911d00",
      "publicKeyJwk": {
        "alg": "RS256",
        "kid": "93cfc125cafc43f5acdbef26e5911d00",
        "kty": "RSA",
        "use": "sig"
      },
      "type": "RsaVerificationKey2018"
    }
  ]
}
```

## Creating a Verifiable Credential and Verification

After the DIDs have been made available the Verifiable Credential can be created.

`ssikit vc issue -t ParticipantCredential -i did:web:vc.lab.gaia-x.eu#8eb4a104005948c886300d40daf7925f -s did:web:delta-dao.com#93cfc125cafc43f5acdbef26e5911d00 --interactive data/vc_2.json`

Example Verifiable Credential

```plaintext
{
  "@context" : [ "https://www.w3.org/2018/credentials/v1", "https://w3id.org/security/suites/ed25519-2020/v1", "https://w3id.org/security/suites/jws-2020/v1" ],
  "credentialSchema" : {
    "id" : "https://raw.githubusercontent.com/walt-id/waltid-ssikit-vclib/master/src/test/resources/schemas/ParticipantCredential.json",
    "type" : "JsonSchemaValidator2018"
  },
  "credentialStatus" : {
    "id" : "https://gaiax.europa.eu/status/participant-credential#392ac7f6-399a-437b-a268-4691ead8f176",
    "type" : "CredentialStatusList2020"
  },
  "credentialSubject" : {
    "companyName" : "deltaDAO AG",
    "companyNumber" : "HRB 170364",
    "headquarterCountry" : "GER",
    "id" : "did:web:delta-dao.com#93cfc125cafc43f5acdbef26e5911d00",
    "legalCountry" : "GER",
    "lei" : "5299004GII7NEZFIFE28",
    "parentOrganisation" : "",
    "subOrganisation" : ""
  },
  "id" : "urn:uuid:43661986-a6b3-49c8-99d5-e3001b9a98e0",
  "issued" : "2022-03-18T21:21:55.918280759Z",
  "issuer" : "did:web:vc.lab.gaia-x.eu#8eb4a104005948c886300d40daf7925f",
  "validFrom" : "2022-03-18T21:21:55.918321801Z",
  "issuanceDate" : "2022-03-18T21:21:55.918321801Z",
  "type" : [ "VerifiableCredential", "ParticipantCredential" ],
  "proof" : {
    "type" : "Ed25519Signature2018",
    "creator" : "did:web:vc.lab.gaia-x.eu#8eb4a104005948c886300d40daf7925f",
    "created" : "2022-03-18T21:21:58Z",
    "domain" : "https://api.preprod.ebsi.eu",
    "nonce" : "48eeb495-416a-4f72-a8ec-e0c738b17aaf",
    "proofPurpose" : "assertion",
    "jws" : "eyJiNjQiOmZhbHNlLCJjcml0IjpbImI2NCJdLCJhbGciOiJFZERTQSJ9..6Ty5C5EtSkFawZOqObVy2euYrIpvBlr4XzRVl0uA1YYg4g_RAePNPBc9Ulh-l70L_9ETFir6REPHL2J3jIOYBg"
  }
}
```

This VC can be validated.

`ssikit vc verify data/vc2.json`

## Creating a Verifiable Presentation and Verification

From the VC a Verifiable Presentation can be created.

`ssikit vc present --holder-did did:web:delta-dao.com#93cfc125cafc43f5acdbef26e5911d00 data/vc2.json`

Example Verifiable Presentation, created from the Verifiable Credential

```plaintext
{
  "@context" : [ "https://www.w3.org/2018/credentials/v1" ],
  "holder" : "did:web:delta-dao.com#93cfc125cafc43f5acdbef26e5911d00",
  "id" : "urn:uuid:cdb447a3-515c-4229-b79a-7fad51a82b83",
  "type" : [ "VerifiablePresentation" ],
  "verifiableCredential" : [ {
    "@context" : [ "https://www.w3.org/2018/credentials/v1", "https://w3id.org/security/suites/ed25519-2020/v1", "https://w3id.org/security/suites/jws-2020/v1" ],
    "credentialSchema" : {
      "id" : "https://raw.githubusercontent.com/walt-id/waltid-ssikit-vclib/master/src/test/resources/schemas/ParticipantCredential.json",
      "type" : "JsonSchemaValidator2018"
    },
    "credentialStatus" : {
      "id" : "https://gaiax.europa.eu/status/participant-credential#392ac7f6-399a-437b-a268-4691ead8f176",
      "type" : "CredentialStatusList2020"
    },
    "credentialSubject" : {
      "companyName" : "deltaDAO AG",
      "companyNumber" : "HRB 170364",
      "headquarterCountry" : "GER",
      "id" : "did:web:delta-dao.com#93cfc125cafc43f5acdbef26e5911d00",
      "legalCountry" : "GER",
      "lei" : "5299004GII7NEZFIFE28",
      "parentOrganisation" : "",
      "subOrganisation" : ""
    },
    "id" : "urn:uuid:43661986-a6b3-49c8-99d5-e3001b9a98e0",
    "issued" : "2022-03-18T21:21:55.918280759Z",
    "issuer" : "did:web:vc.lab.gaia-x.eu#8eb4a104005948c886300d40daf7925f",
    "proof" : {
      "created" : "2022-03-18T21:21:58Z",
      "creator" : "did:web:vc.lab.gaia-x.eu#8eb4a104005948c886300d40daf7925f",
      "domain" : "https://api.preprod.ebsi.eu",
      "jws" : "eyJiNjQiOmZhbHNlLCJjcml0IjpbImI2NCJdLCJhbGciOiJFZERTQSJ9..6Ty5C5EtSkFawZOqObVy2euYrIpvBlr4XzRVl0uA1YYg4g_RAePNPBc9Ulh-l70L_9ETFir6REPHL2J3jIOYBg",
      "nonce" : "48eeb495-416a-4f72-a8ec-e0c738b17aaf",
      "proofPurpose" : "assertion",
      "type" : "Ed25519Signature2018"
    },
    "validFrom" : "2022-03-18T21:21:55.918321801Z",
    "issuanceDate" : "2022-03-18T21:21:55.918321801Z",
    "type" : [ "VerifiableCredential", "ParticipantCredential" ]
  } ],
  "proof" : {
    "type" : "RsaSignature2018",
    "creator" : "did:web:delta-dao.com#93cfc125cafc43f5acdbef26e5911d00",
    "created" : "2022-03-20T12:17:45Z",
    "domain" : "https://api.preprod.ebsi.eu",
    "nonce" : "a8caadb6-00f2-4453-a8dc-6934c63b834a",
    "proofPurpose" : "authentication",
    "verificationMethod" : "did:web:delta-dao.com#93cfc125cafc43f5acdbef26e5911d00",
    "jws" : "eyJiNjQiOmZhbHNlLCJjcml0IjpbImI2NCJdLCJhbGciOiJSUzI1NiJ9..kTa8D8ir89anjQMBFItnfj6Kd91WLMYR7HWUfAg4DTjIz19j7DfCcz0bwiBcAnDKvpnvXS6CD-FPP4_PINYUMrhPQ0VaivXe9VV9TBc_PWhT7hgzK990id7t914rRPNaPSbCeEzHYe1rTPksmPE7EPS0pC1IYvnlp__-NwvKP_GXtK0Rg4mIoVa8ElOpf6VAPwzA4Vg04Aok5QGk_5gqAcVqFeaRtXaJBKCWkrPOeH5R9-_41vRPLIIpZ-M9yiewMNIW9Ftwe3qFMHdNXRT6V7Sjv52tfANZbulBmX-r8EGZinKxHm7-G3W76joHXwETB_wgcHwM38NpdhXbNhUk9Q"
  }
}
```

The VP can be checked against different policies.

`ssikit vc verify data/vc/presented/vp-1647778665763.json -p SignaturePolicy -p JsonSchemaPolicy`

The last check against the Gaia-X Registry is still work in progress.