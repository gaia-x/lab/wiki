This is the initial list of solutions for evaluation in the Gaia-X Lab, as outlined in [Evaluation of SSI Solutions](https://gitlab.com/groups/gaia-x/lab/-/wikis/Evaluation-of-SSI-Solutions).

This list helps to identity suitable kits and components for the Trust & Identity service.

This list is, by any means, incomplete and work in progress. It will be updated on a regular basis as work progresses and community feedback and evaluation results enter the Gaia-X Lab.

These solutions can be roughly categorized in 5 different groups:

- SSI Kits
- Resolvers
- Infrastructures / Ecosystems
- Wallets
- Others

## TRL

![image](uploads/a1b2cf7d836fc5bc51273b1c2eb93b52/image.png)

Technology Readiness Levels Framework

## 1. SSI Kits / SDKs

| Name | Website | Services | Repositories | Ecosystem | Open Source | Languages | TRL |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| walt.id | [Website](https://walt.id/) | SSI Kit, OIDC/SIOPv2 | [GitHub](https://github.com/walt-id) | EBSI, WEB, KEY | Apache-2.0 | Vue, Kotlin, Typescript | 5-6 |
| DIDKit | [Website](https://spruceid.dev/docs/didkit/) | SSI Kit? | [GitHub](https://github.com/spruceid/didkit) | KEY, WEB, ETHR, many | Apache-2.0 | Rust, Dart, Javascript | 5-6 |  

## 2. Resolvers / Methods

| Name | Website | Services | Repositories | Ecosystem | Open Source | Languages | TRL |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| LACChain Uniresolver | [Website](https://www.lacchain.net/) | Uniresolver, DIDComm | [GitHub](https://github.com/lacchain/universal-resolver) | ETHR, LAC | Apache-2.0 | Javascript | 9 |
| DIF Uniresolver | [Website](https://identity.foundation/) | Unirevolver, DIDComm | [GitHub](https://github.com/decentralized-identity/universal-resolver) | many | Apache-2.0 | Javascript, Java, Rust, Typescript | 5-7 |
| Veramo UPort | [Website](https://veramo.io/) | Uniresolver, DIDComm | [GitHub](https://github.com/uport-project) | many | Apache-2.0 | Javascript, Typescript, Kotlin, Swift, C | 5-7 |
| BCdiploma/EvidenZ | [Website](https://www.bcdiploma.com/en) | Resolver, SaaS | [GitHub](https://github.com/VinceBCD/BCDiploma) | ETHR | Commercial? | Javascript, Typescript, Solidity | 7-9 |
| Microsoft ION | [Website](https://identity.foundation/ion/) | Issuer, Resolver | [GitHub](https://github.com/decentralized-identity/ion) | BTCR | Apache-2.0 | Javascript, Typescript | 4-5 |

## 3. Infrastructures / Ecosystems

| Name | Website | Services | Repositories | Ecosystem | Open Source | Languages | TRL |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| LACChain | [Website](https://www.lacchain.net/) | Infrastructure, Registry | [GitHub](https://github.com/lacchain) | besu, public-permissioned | Apache-2.0 | Javascript, Java, Go | 9 |
| sovrin | [Website](https://sovrin.org/) | Infrastructure, Registry | [GitHub](https://github.com/sovrin-foundation) | aries, sovrin, public-permissioned | Apache-2.0 | Python, Rust, Vue | 7-9 |
| EBSI | [Website](https://ec.europa.eu/cefdigital/wiki/display/CEFDIGITAL/EBSI) | Infrastructure, Registry | [docs](https://ec.europa.eu/cefdigital/wiki/display/EBSIDOC/EBSI+Documentation+Home) | public-permissioned | yes | Solidity, other | 5-7 |
| IDUnion | [Website](https://idunion.org/?lang=en) | Infrastructure, Registry | [GitHub](https://github.com/IDunion) | indy, public-permissioned | Apache-2.0, MIT, ? | Python, Javascript, Rust | 5-7 |
| Alastria | [Website](https://www.alastria.io/#) | Infrastructure, Registry | [GitHub](https://github.com/alastria/alastria-identity) | besu/fabric, public-permissioned | Apache-2.0, MIT | Javascript, Typescript, Solidity | 5-7 |

## 4. Wallets

| Name | Website | Services | Repositories | Ecosystem | Open Source | Languages | TRL |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| Talao Wallet | [Website](https://www.talao.io/) | Wallet, Issuer, Resolver | [GitHub](https://github.com/TalaoDAO/talao-wallet) | WEB, KEY, ETHR, TZ, (SIOPv2) | Apache-2.0 | Javascript, Flutter | 4-6 |
| Spruce Credible | [Website](https://spruceid.dev/docs/credible/) | Wallet | [GitHub](https://github.com/spruceid/credible) | WEB, KEY, ETHR, (SIOPv2) | Apache-2.0 | Javascript, Flutter | 4-5 |
| Trinsic | [Website](https://trinsic.id/) | - | [GitHub](https://github.com/trinsic-id) | - | No | - | - |
| Jolocom | [Website](https://jolocom.io/) | - | [GitHub](https://github.com/jolocom/smartwallet-app) | many | Yes | - | - |
| Lissi | [Website](https://lissi.id/) | - | - | Aries | No | - | - |
| Esatus | [Website](https://esatus.com/) | - | [GitHub](https://github.com/esatus) | Aries | No | - | - |
| Procivis | [Website](https://www.procivis.ch/) | - | [GitHub](https://github.com/procivis) | Aries | No | - | - |

## 5. Others

| Name | Website | Services | Repositories | Ecosystem | Open Source | Languages | TRL |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| Sphereon | [Website](https://sphereon.com/) | SIOP Authenticator | [GitHub](https://github.com/Sphereon-Opensource/ssi-sdk/blob/develop/packages/did-auth-siop-op-authenticator/README.md) | ETHR, many, Uniresolver | Apache-2.0 | Typescript | 4-5 |



