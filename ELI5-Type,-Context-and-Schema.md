# Type, Context and Schema in Verifiable Credentials

The @context provides an array of URIs to identity means that allow users to utilize the content of a Verifiable Credential.

Fields in the VC are complementary and mutually exclusive with each other and the definition in the W3C repository.

This means that each type of VC specified must have a link to the context defining the standard (in this case the W3C Verifiable Credential Data Model 1.0) and to the context defining the fields that extend the VC, which is usually in the “credentialSubject” field

Example:

```plaintext
{
  "@context": [
    "https://www.w3.org/ns/did/v1",
    "https://w3id.org/security/suites/ed25519-2020/v1"
  ]
  "id": "did:example:123456789abcdefghi",
  "verificationMethod": [{
    "id": "did:example:123456789abcdefghi#key-1",
    "type": "Ed25519VerificationKey2020", 
    "controller": "did:example:123456789abcdefghi",
    "publicKeyMultibase": "zH3C2AVvLMv6gmMNam3uVAjZpfkcJCwDwnZn6z3wXmqPV"
  }, ...],
```

Here the context specifies how to deal with the type <span dir="">"</span>Ed25519VerificationKey2020<span dir="">"</span>, see <https://digitalbazaar.github.io/ed25519-signature-2020-context/contexts/ed25519-signature-2020-v1.jsonld>

```plaintext
"Ed25519VerificationKey2020": {
      "@id": "https://w3id.org/security#Ed25519VerificationKey2020",
      "@context": {
        "@protected": true,
        "id": "@id",
        "type": "@type",
        "controller": {
          "@id": "https://w3id.org/security#controller",
          "@type": "@id"
        },
        "revoked": {
          "@id": "https://w3id.org/security#revoked",
          "@type": "http://www.w3.org/2001/XMLSchema#dateTime"
        },
        "publicKeyMultibase": {
          "@id": "https://w3id.org/security#publicKeyMultibase",
          "@type": "https://w3id.org/security#multibase"
        }
      }
    },
```

When extending the context property names and values are expected to be registered in DID specification registries (see: <https://www.w3.org/TR/did-spec-registries/>) with links to a formal definition and a JSON-LD context.

Extension of context can be done in the document like this

{ “@context”: \[ { “@version”: 1.1 },” [https://www.w3.org/ns/odrl.jsonld”](https://www.w3.org/ns/odrl.jsonld%E2%80%9D) , { “la”: “ [https://lacchain.net/credentials/library/education”](https://lacchain.net/credentials/library/education%E2%80%9D) , “schema”: “ [http://schema.org/”](http://schema.org/%E2%80%9D) , “rdf”: “ [http://www.w3.org/1999/02/22-rdf-syntax-ns#”](http://www.w3.org/1999/02/22-rdf-syntax-ns#%E2%80%9D) , “givenName”: “la:givenName”, “familyName”: “la:familyName”, “email”: “la:email”, “holds”: “la:holds”, “category”: “la:category”, “industry”: “la:industry”, “skillset”: “la:skillset”, “course”: “la:course”, “description”: “la:description”, “duration”: “la: duration”, “modality”: “la:modality”, “location”: “la: location” }\] “url”: “la:url” }

Here we find a link to a "Schema" as well.

**What is the difference between the context and the schema?**

The [**@context**](https://github.com/context) property defines a common vocabulary. It allows to have a string like `VerifiableCredential` as a value, which a processor can map to the value [`https://w3.org/2018/credentials#VerifiableCredential`\
](https://w3.org/2018/credentials#VerifiableCredential%EF%BF%BCReally)A context just defines a dictionary of name-value pairs.

The **credentialSchema** property defines a data schema, i.e. a schema that defines the structure of the data in the credential. The data schema would define the property names and acceptable value types one might expect to find in the credentialSubject section. If the VC should contain a `hireDate` property that should be of type `RFC3339` the **credentialSchema** is where that would be specified.

<span dir="">credentialSchema MUST be one or more data schemas that provide verifiers with enough information to determine if the provided data conforms to the provided schema. Each credentialSchema MUST specify its type</span>. 

<span dir="">The credentialSchema and type properties are very similar in what they may contain, but the type property doesn't actually need to contain anything, while the credentialSchema does.</span>

<span dir="">CredentialSchema defines the properties AND value types that should be expected in the credential. This allows a credential to be **automatically processed** to see if the properties and values it contains match the properties and value types specified in the credentialSchema.</span>

The type itself is a URL (when resolved via the mappings in `@context`). To define the type itself you can:

1. Specify it in some human readable spec and require people to read it and add the rules to their application. (This should probably be considered legacy approach).
2. Return machine readable information from the type URL that directly or indirectly (through "follow your nose" linked data) specifies the information.
3. Specify a `credentialSchema` property with a vocabulary document and/or schema that has the information.

<span dir="">The latter two mechanisms can include content integrity proofs via other technologies like hashlinks or DLT</span>.

<span dir="">The benefit of the credentialSchema property over the </span>[**@context**](https://github.com/context)<span dir=""> property (ignoring the data encoding schema which </span>[**@context**](https://github.com/context)<span dir=""> does not cover) is that it provides direct links to the schema definitions rather than possibly many levels of indirection via the </span>[**@context**](https://github.com/context)<span dir=""> property and it provides an opportunity to annotate type definitions or "lock them" to specific versions of the vocabulary.</span> <span dir="">Depending on how stable and popular such a vocabulary is, authors of VCs may prefer to include a "static" version of their vocabulary via </span>`credentialSchema`<span dir=""> that is locked to some content integrity hash.</span>

References:

* <https://w3c.github.io/vc-data-model/#contexts>
* <https://www.w3.org/TR/json-ld/#the-context>
* <https://w3c.github.io/vc-data-model/#content-integrity-protection>