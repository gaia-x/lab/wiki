## Purpose of the document

Documentation of a complete participant credential in line with the latest specifications of the Architecture Document (https://gaia-x.gitlab.io/technical-committee/architecture-document//self-description/), the work of the SD Working Group (https://gitlab.com/gaia-x/gaia-x-community/gaia-x-self-descriptions/-/tree/master/implementation/validation) and the feedback of the SD Lifecycle Working Group (Julius Pfrommer).

This builds on the initial example SD that can be found here: https://gitlab.com/groups/gaia-x/lab/-/wikis/Quick-Guide-CLI-Credential-Creation-and-Verification-(WIP)

The Verifiable Credential builds on the data schema https://www.w3.org/TR/vc-data-model/#data-schemas in the Verifiable Credentials Data Model 1.1 and the current specifications of the Trust Framework https://gaia-x.gitlab.io/policy-rules-committee/trust-framework/

## First Example

```
{
  "@context" : [ "https://www.w3.org/2018/credentials/v1",
                 "https://w3id.org/security/suites/ed25519-2020/v1",
                 "https://w3id.org/security/suites/jws-2020/v1" ],
  "credentialSchema" : {
    "id" : "https://raw.githubusercontent.com/walt-id/waltid-ssikit-vclib/master/src/test/resources/schemas/ParticipantCredential.json",
    "type" : "JsonSchemaValidator2018"
  },
  "credentialStatus" : {
    "id" : "https://gaiax.europa.eu/status/participant-credential#392ac7f6-399a-437b-a268-4691ead8f176",
    "type" : "CredentialStatusList2020"
  },
  "credentialSubject" : {
    "ethereumAddress" : "0x4C84a36fCDb7Bc750294A7f3B5ad5CA8F74C4A52",
    "hasCountry" : "GER",
    "hasJurisdiction" : "GER",
    "hasLegallyBindingName" : "deltaDAO AG",
    "hasRegistrationNumber" : "DEK1101R.HRB170364",
    "id" : "did:web:delta-dao.com#93cfc125cafc43f5acdbef26e5911d00",
    "leiCode" : "391200FJBNU0YW987L26",
    "parentOrganisation" : "",
    "subOrganisation" : ""
  },
  "id" : "urn:uuid:a45fb295-b08c-45e8-8a92-2df43f7e7b4b",
  "issued" : "2022-05-06T09:24:24.562972344Z",
  "issuer" : "did:web:vc.lab.gaia-x.eu#8eb4a104005948c886300d40daf7925f",
  "validFrom" : "2022-05-06T09:24:24.563008553Z",
  "issuanceDate" : "2022-05-06T09:24:24.563008553Z",
  "type" : [ "VerifiableCredential", "ParticipantCredential" ],
  "proof" : {
    "type" : "Ed25519Signature2018",
    "creator" : "did:web:vc.lab.gaia-x.eu#8eb4a104005948c886300d40daf7925f",
    "created" : "2022-05-06T09:24:51Z",
    "domain" : "https://api.preprod.ebsi.eu",
    "nonce" : "c2eba84d-c98d-48e8-88cb-6a4146058eb7",
    "proofPurpose" : "assertion",
    "jws" : "eyJiNjQiOmZhbHNlLCJjcml0IjpbImI2NCJdLCJhbGciOiJFZERTQSJ9..hD7edInsm6ThndMIMZXbIXtsFUU_TpwAMcyeSpB3briDBTtMXF2dLCC-1HsfKlyc0xpl3KQfasGwe2i9lf2-Bw"
  }
}
```

## First feedback / issues and necessary adjustments

Issue: We want to use the JWS2020 signature suite for its compatibility with X509 RSA keys.
Response: Agreed. Using https://w3id.org/security/suites/jws-2020/v1 

Issue: The way I read the VC Status spec (https://w3c-ccg.github.io/vc-status-list-2021/) you must give the statusPurpose and that must be either “revocation” or “suspension”.
Response: According to https://w3c-ccg.github.io/vc-status-list-2021/ "When an issuer desires to enable status information for a verifiable credential, they MAY add a status property that uses the data model described in this specification." This leads to the inclusion of the id, the type, the purpose, the listindex and the listcredential. I am not sure if we want to integrate the status for the minimal credential right away, so we might skip the completion of this for now. However, the Registry might feature this soon.

Issue: Is it okay to use any web URI for the schema identifier or does it have to live in a specific domain-namespace?
Answer: There is no specific domain-namespace, it can be any web URI. It would certainly be under the governance of the Gaia-X AISBL / community.

Issue: For the minimal Self-Description we should only include the mandatory fields.
Answer: Agree. Makes it simpler and concise.

Issue: Please use something resolvable like a did-web for the unique Gaia-X identifier.
Answer: did:web:vc.lab.gaia-x.eu and did:web:delta-dao.com can be resolved via Uniresolver https://dev.uniresolver.io/. did:web:vc.lab.gaia-x.eu is using ED25519 while deltaDAO uses RSA to make the connection to the X.509 and eIDAS world later on.

Issue: The proof must contain a proofPurpose field, but it is missing.
Answer: "proofPurpose" : "assertion" will be changed to "proofPurpose" : "assertionMethod"

Issue: Why use the “jws” field instead of the proofValue field?
Answer: Both are valid, depending on the security suite that is used. Example: https://www.w3.org/TR/vc-data-model/#example-a-simple-example-of-a-verifiable-presentation where RsaSignature2018 is used. We will revert to using proofPurpose.

Issue: `type`: The field is set to `"Ed25519Signature2020"`.
Answer: We agreed to use ED25519 for the Lab and RSA for the holders in the early beginning, to make the connection towards the X.509 certificates. Any Signature can be used for the issuer and the holder. We'll now update to RSA usage.

Issue: `proofPurpose`: The field is set to `"assertionMethod"`.
Answer: Will be updated.

Issue: `verificationMethod`: The field identifies the party that has issued the proof. It contains either a) the GAIA-X Identifier of the Participant that is signing or b) the digest (fingerprint) of an X509 certificate containing the key material. The GAIA-X Identifier of the signing party can be resolved to a Self-Description that contains the public key used for the signature. An X509 certificate digest is to be matched to known certificates to resolve the public key. For example certificates from the GAIA-X Registry.
Answer: Agreed. For X.509 the certificate needs to be made available via PKI to verify it against the registry and the list of trust anchors. For this reason .p7b or .cer files will be investigated and certificates 

Issue: `created`: The field contains the creation date in the ISO8601 format.
Answer: yes.

Issue: `proofValue`: The field contains the cryptographic signature itself.
Answer: yes.

Issue: You have 3 date-fields that are all identical. Let’s settle on just one of them for the “minimal SD”. I think the (mandatory) created-field in the proof is enough.
Answer: The proof will need the created attribute while the credential will need the issueanceDate attribute. validFrom and issues can most likely be removed.

Issue: A link to a valid X.509 eIDAS certificate and signature is needed to make the connection to an TSP issued identity
Answer: tbd how to facilitate at a later point.

## Updates (before refinement):
Shape: https://registry.lab.gaia-x.eu/shapes/v1/participant.json

## Ordered Issues (mandatory changes)

1. Must: Empty attributes need to be removed from the Credential
- "parentOrganisation"
- "subOrganisation"
2. Optional: id contains a uuid and needs to be changed to DID. See: https://www.w3.org/TR/vc-data-model/#example-basic-structure-of-a-presentation
3. New: use RSA signatures for demonstration purposes.
4. Must: Remove "domain" : "https://api.preprod.ebsi.eu"
5. Optional: 

## New Example for a Participant Self-Description as Verifiable Credential (VC)

It is not created through code, but manually. Please don't try to verify the signatures here.

```
{
  "@context" : [ "https://www.w3.org/2018/credentials/v1", "https://w3id.org/security/v1", "https://w3id.org/security/suites/jws-2020/v1", "http://w3id.org/gaia-x/participant" ],
  "credentialSchema" : {
    "id" : "https://vc.lab.gaia-x.eu/schemas/ParticipantCredentialv1.json",
    "type" : "JsonSchemaValidator2018"
  },
  "credentialStatus" : {
    "id" : "https://vc.lab.gaia-x.eu/credentialstatus/392ac7f6-399a-437b-a268-4691ead8f176",
    "type" : "StatusList2021Entry"
    "statusPurpose" : "revocation"
  },
  "credentialSubject" : {
    "hasCountry" : "GER",
    "hasJurisdiction" : "GER",
    "hasLegallyBindingName" : "deltaDAO AG",
    "hasRegistrationNumber" : "DEK1101R.HRB170364",
    "id" : "did:web:delta-dao.com#93cfc125cafc43f5acdbef26e5911d00",
    "leiCode" : "391200FJBNU0YW987L26",
  },
  "type" : [ "VerifiableCredential", "ParticipantCredential" ],
  "id" : "https://vc.lab.gaia-x.eu/credentials/392ac7f6-399a-437b-a268-4691ead8f176",
  "issuer" : "did:web:vc.lab.gaia-x.eu#8eb4a104005948c886300d40daf7925f",
  "issuanceDate" : "2022-05-06T09:24:24.563008553Z",
  "proof" : {
    "type" : "RsaSignature2018",
    "created" : "2022-05-06T09:24:51Z",
    "proofPurpose" : "assertionMethod",
    "verificationMethod": "https://vc.lab.gaia-x.eu/issuers/keys/1",
    "proofValue" : "eyJhbGciOiJSUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..TCYt5X
    sITJX1CxPCT8yAV-TVkIEq_PbChOMqsLfRoPsnsgw5WEuts01mq-pQy7UJiN5mgRxD-WUc
    X16dUEMGlv50aqzpqh4Qktb3rk-BuQy72IFLOqV0G_zS245-kronKb78cPN25DGlcTwLtj
    PAYuNzVBAh4vGHSrQyHUdBBPM"
  }
}
```

## New Example for a Participant Self-Description the Compliance Service

```
{
  "selfDescription": {
    "@context": {
      "sh": "http://www.w3.org/ns/shacl#",
      "xsd": "http://www.w3.org/2001/XMLSchema#",
      "gx-participant": "http://w3id.org/gaia-x/participant#"
    },
    "@id": "http://example.org/participant-dp6gtq7i75lmk9p4j2tfg",
    "@type": "gx-participant:LegalPerson",
    "gx-participant:registrationNumber": {
      "@type": "xsd:string",
      "@value": "DEANY1234NUMBER"
    },
    "gx-participant:legalAddress": {
      "@type": "gx-participant:Address",
      "gx-participant:country": {
        "@type": "xsd:string",
        "@value": "DEU"
      },
      "gx-participant:state": {
        "@type": "xsd:string",
        "@value": "NRW"
      }
    },
    "gx-participant:headquarterAddress": {
      "@type": "gx-participant:Address",
      "gx-participant:country": {
        "@type": "xsd:string",
        "@value": "DEU"
      }
    },
    "gx-participant:leiCode": {
      "@type": "xsd:string",
      "@value": "391200FJBNU0YW987L26"
    },
    "gx-participant:parentOrganisation": [
      {
        "@type": "gx-participant:LegalPerson",
        "gx-participant:registrationNumber": {
          "@type": "xsd:string",
          "@value": "DE12345678"
        },
        "gx-participant:legalAddress": {
          "@type": "gx-participant:Address",
          "gx-participant:country": {
            "@type": "xsd:string",
            "@value": "DEU"
          }
        },
        "gx-participant:headquarterAddress": {
          "@type": "gx-participant:Address",
          "gx-participant:country": {
            "@type": "xsd:string",
            "@value": "DE"
          }
        }
      },
      {
        "@type": "gx-participant:LegalPerson",
        "gx-participant:registrationNumber": {
          "@type": "xsd:string",
          "@value": "DE98761234"
        },
        "gx-participant:legalAddress": {
          "@type": "gx-participant:Address",
          "gx-participant:country": {
            "@type": "xsd:string",
            "@value": "DE"
          }
        },
        "gx-participant:headquarterAddress": {
          "@type": "gx-participant:Address",
          "gx-participant:country": {
            "@type": "xsd:string",
            "@value": "DE"
          }
        }
      }
    ],
    "gx-participant:subOrganisation": {
      "@type": "gx-participant:LegalPerson",
      "gx-participant:registrationNumber": {
        "@type": "xsd:string",
        "@value": "DESUB1234COMPANY"
      },
      "gx-participant:legalAddress": {
        "@type": "gx-participant:Address",
        "gx-participant:country": {
          "@type": "xsd:string",
          "@value": "DE"
        }
      },
      "gx-participant:headquarterAddress": {
        "@type": "gx-participant:Address",
        "gx-participant:country": {
          "@type": "xsd:string",
          "@value": "DEU"
        }
      }
    }
  },
  "proof": {
    "type": "RsaSignature2018",
    "created": "2022-05-17T06:42:15.732Z",
    "proofPurpose": "assertionMethod",
    "verifcationMethod": "-----BEGIN PUBLIC KEY----- MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAxHETOBbHODQmN0uc/+b8 wh6JPi7Q5YEPmY7O4K0K3FcwVJ8t+jyxoa/BUBPb2aH9g1hOZhCxTjq2mDaMKSnq MzqBcBBXqlhbPte8XTTWu/Rx0W3RPuGmiWaAPIb99okLhfcgU6jRekljhyluxFK3 aJFlFF+zYaltFV0Jq3vfKARaiB6OtXV9EzMvNE8Zs6rC4oiROy5ZgNCrBHH6KtBX 3YJOdmLiZLGTcVTSvhoSEPKKQGxWdE6EYCYtAehPljafRVKKd3KVdZVaXolDbHb+ hIFnSQdLxx9Q18i1q28QpuziLmkRrNFA9vCbjt/a2VBWjw+GgRIPNx0ANJCkVLht hQIDAQAB -----END PUBLIC KEY-----",
    "jws": "eyJhbGciOiJQUzI1NiJ9.eyJzZWxmRGVzY3JpcHRpb24iOnsiQGNvbnRleHQiOnsic2giOiJodHRwOi8vd3d3LnczLm9yZy9ucy9zaGFjbCMiLCJ4c2QiOiJodHRwOi8vd3d3LnczLm9yZy8yMDAxL1hNTFNjaGVtYSMiLCJneC1wYXJ0aWNpcGFudCI6Imh0dHA6Ly93M2lkLm9yZy9nYWlhLXgvcGFydGljaXBhbnQjIn0sIkBpZCI6Imh0dHA6Ly9leGFtcGxlLm9yZy9wYXJ0aWNpcGFudC1kcDZndHE3aTc1bG1rOXA0ajJ0ZmciLCJAdHlwZSI6Imd4LXBhcnRpY2lwYW50OkxlZ2FsUGVyc29uIiwiZ3gtcGFydGljaXBhbnQ6cmVnaXN0cmF0aW9uTnVtYmVyIjp7IkB0eXBlIjoieHNkOnN0cmluZyIsIkB2YWx1ZSI6IkRFQU5ZMTIzNE5VTUJFUiJ9LCJneC1wYXJ0aWNpcGFudDpsZWdhbEFkZHJlc3MiOnsiQHR5cGUiOiJneC1wYXJ0aWNpcGFudDpBZGRyZXNzIiwiZ3gtcGFydGljaXBhbnQ6Y291bnRyeSI6eyJAdHlwZSI6InhzZDpzdHJpbmciLCJAdmFsdWUiOiJERVUifSwiZ3gtcGFydGljaXBhbnQ6c3RhdGUiOnsiQHR5cGUiOiJ4c2Q6c3RyaW5nIiwiQHZhbHVlIjoiTlJXIn19LCJneC1wYXJ0aWNpcGFudDpoZWFkcXVhcnRlckFkZHJlc3MiOnsiQHR5cGUiOiJneC1wYXJ0aWNpcGFudDpBZGRyZXNzIiwiZ3gtcGFydGljaXBhbnQ6Y291bnRyeSI6eyJAdHlwZSI6InhzZDpzdHJpbmciLCJAdmFsdWUiOiJERVUifX0sImd4LXBhcnRpY2lwYW50OmxlaUNvZGUiOnsiQHR5cGUiOiJ4c2Q6c3RyaW5nIiwiQHZhbHVlIjoiMzkxMjAwRkpCTlUwWVc5ODdMMjYifSwiZ3gtcGFydGljaXBhbnQ6cGFyZW50T3JnYW5pc2F0aW9uIjpbeyJAdHlwZSI6Imd4LXBhcnRpY2lwYW50OkxlZ2FsUGVyc29uIiwiZ3gtcGFydGljaXBhbnQ6cmVnaXN0cmF0aW9uTnVtYmVyIjp7IkB0eXBlIjoieHNkOnN0cmluZyIsIkB2YWx1ZSI6IkRFMTIzNDU2NzgifSwiZ3gtcGFydGljaXBhbnQ6bGVnYWxBZGRyZXNzIjp7IkB0eXBlIjoiZ3gtcGFydGljaXBhbnQ6QWRkcmVzcyIsImd4LXBhcnRpY2lwYW50OmNvdW50cnkiOnsiQHR5cGUiOiJ4c2Q6c3RyaW5nIiwiQHZhbHVlIjoiREVVIn19LCJneC1wYXJ0aWNpcGFudDpoZWFkcXVhcnRlckFkZHJlc3MiOnsiQHR5cGUiOiJneC1wYXJ0aWNpcGFudDpBZGRyZXNzIiwiZ3gtcGFydGljaXBhbnQ6Y291bnRyeSI6eyJAdHlwZSI6InhzZDpzdHJpbmciLCJAdmFsdWUiOiJERSJ9fX0seyJAdHlwZSI6Imd4LXBhcnRpY2lwYW50OkxlZ2FsUGVyc29uIiwiZ3gtcGFydGljaXBhbnQ6cmVnaXN0cmF0aW9uTnVtYmVyIjp7IkB0eXBlIjoieHNkOnN0cmluZyIsIkB2YWx1ZSI6IkRFOTg3NjEyMzQifSwiZ3gtcGFydGljaXBhbnQ6bGVnYWxBZGRyZXNzIjp7IkB0eXBlIjoiZ3gtcGFydGljaXBhbnQ6QWRkcmVzcyIsImd4LXBhcnRpY2lwYW50OmNvdW50cnkiOnsiQHR5cGUiOiJ4c2Q6c3RyaW5nIiwiQHZhbHVlIjoiREUifX0sImd4LXBhcnRpY2lwYW50OmhlYWRxdWFydGVyQWRkcmVzcyI6eyJAdHlwZSI6Imd4LXBhcnRpY2lwYW50OkFkZHJlc3MiLCJneC1wYXJ0aWNpcGFudDpjb3VudHJ5Ijp7IkB0eXBlIjoieHNkOnN0cmluZyIsIkB2YWx1ZSI6IkRFIn19fV0sImd4LXBhcnRpY2lwYW50OnN1Yk9yZ2FuaXNhdGlvbiI6eyJAdHlwZSI6Imd4LXBhcnRpY2lwYW50OkxlZ2FsUGVyc29uIiwiZ3gtcGFydGljaXBhbnQ6cmVnaXN0cmF0aW9uTnVtYmVyIjp7IkB0eXBlIjoieHNkOnN0cmluZyIsIkB2YWx1ZSI6IkRFU1VCMTIzNENPTVBBTlkifSwiZ3gtcGFydGljaXBhbnQ6bGVnYWxBZGRyZXNzIjp7IkB0eXBlIjoiZ3gtcGFydGljaXBhbnQ6QWRkcmVzcyIsImd4LXBhcnRpY2lwYW50OmNvdW50cnkiOnsiQHR5cGUiOiJ4c2Q6c3RyaW5nIiwiQHZhbHVlIjoiREUifX0sImd4LXBhcnRpY2lwYW50OmhlYWRxdWFydGVyQWRkcmVzcyI6eyJAdHlwZSI6Imd4LXBhcnRpY2lwYW50OkFkZHJlc3MiLCJneC1wYXJ0aWNpcGFudDpjb3VudHJ5Ijp7IkB0eXBlIjoieHNkOnN0cmluZyIsIkB2YWx1ZSI6IkRFVSJ9fX19fQ.k11x9g8bc81FW_CvJb_R6h2rDHnjmp_Wa5TKyOPTGL1Ez2UrkZRApD4Rvt7XgYJeEHLDI0xlv7p3VD7rT0sd9eEQqC9jH-cmtRTr8RLywvX99SOhXCKVJgWZPGAEzEXRmG90F2E2Y0PaEQb1hQeikEtHoNcgeRMpQfB6XXIoe7qARPIY-N8QiGP0DsxeosHB91Y2UxSWvuKYYi8nh7O54PgKgT0dDZ-jo24j7RXtyBet3GbBd8jTXPSQRCBX4h7jBt_aJnN-LvF4SsmLMkwdAvtwXVn5eW6piPEDW2rANDT1CJFyEoRcKMAjdA1y9EdTX8fejm4WcTSlJtH57nyiJQ"
  }
}
```
