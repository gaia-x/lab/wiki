## 1. Scope

* Evaluate SSI solutions / technology stacks / ecosystems
* Prioritize solutions for prototyping and further analysis
* Align with existing solutions (GXFS Tenders) and working groups

Following the mission of the Gaia-X Lab to 

* deploy prototypes to technically validate functional and technical hypothesis
* demonstrate working prototypes
* accelerate the development of external OpenSource Software (OSS) projects and to
* identify missing functional components in the Gaia-X Specifications
* identify missing specifications

there is the need to evaluate available SSI solutions and architectures for the first working demonstrators of the

* Accreditation and Onboarding Workflow
* Issuance of signed Self-Descriptions and Life-Cycle Management
* Compliance Service
* Gaia-X Labeling Service

## 2. Role of SSI

Gaia-X Self-Descriptions describe in a machine interpretable format any of the entities of the Gaia-X Conceptual Model. It means that there are Self-Descriptions for all Participant’s Roles: Consumer, Federator, Provider and all the other entities in an Ecosystem’s scope such as Resource and Service Offering. Each Gaia-X entity makes Claims, which are validated and signed by 3rd parties. Those signed Claims are defined as Verifiable Credentials and presented by the entity as Verifiable Presentations. The Verifiable Credentials are issued by other Participants, including Conformity Assessment Bodies (CABs).

Reference: Gaia-X Architecture Chapter 4.6 "Gaia-X Self Description"

Furthermore the Gaia-X identity shall be based on eIDAS regulation or LESS (legally-enabled self-sovereign) identity. Anonymity will not be possible, at least for Federators and Providers.

- Minimum Disclosure
- Full Control
- Necessary Proofs
- Legally-enabled
 
The underlying Gaia-X SSI solution and Verifiable Credentials will be the basis for the Gaia-X Trust & Identity Service, the Gaia-X Compliance and the Gaia-X Labels.

As introduction to SSI we recommend [The Path to Self-Sovereign Identity by Christopher Allen](http://www.lifewithalacrity.com/2016/04/the-path-to-self-soverereign-identity.html) and the guiding principles to SSI.

1. Existence — Users must have an independent existence.
2. Control — Users must control their identities.
3. Access — Users must have access to their own data
4. Transparency — Systems and algorithms must be transparent.
5. Persistence — Identities must be long-lived.
6. Portability — Information and services about identity must be transportable
7. Interoperability — Identities should be as widely usable as possible.
8. Consent — Users must agree to the use of their identity.
9. Minimization — Disclosure of claims must be minimized
10. Protection — The rights of users must be protected

## 3. Initial Situation / Build or Buy?

There are many SSI solutions available. There are different network alternatives available.

A number of SSI solutions use distributed ledger technology, a number of solutions do not use blockchain technology, i.e. DID:WEB, DID:KEY, etc.

If ledger isn’t used, then every issuer of a credential has to maintain infrastructure or contract service providers to respond to DID resolution & revocation requests. And, credential issuers would know when the credential was used, impacting privacy. [13]

Many solutions only offer a partial implementation.

It is very comfortable for our purposes if the solution offers components for the "Issuer", the "Verifier" and the "Wallet" as building a custom identity ecosystem from scratch is a challenging, expensive and high-risk endeavor.

A viable solution should implement a more or less standardized workflow like OIDC/SIOP or DIDComm to be interoperable with other solutions.

Several implementations use closed source components. This applies to many/most of the mature implementations in production, however there is an highly active SSI open-source community that continuously contributes to the evolving SSI technology stack and standards.

For the purpose of rapid prototyping a reproducible implementation and to support understanding of SSI technology it is favorable to reuse existing open-source components that be reused and further developed by the Gaia-X community. 

Building Self-Sovereign Identity (SSI) solutions from scratch is an uneconomical and (in most
cases) even infeasible undertaking because it requires extensive knowledge about a number of
novel and fast-changing technologies (e.g. DLTs, DPKIs,, cryptography, authentication protocols,
etc.) and new standards (e.g. DIDs, VCs). The likely result will be months to years of development
with a high risk of failure. [3]

### 3.1 List of Solutions

A list of components and solutions for rapid prototyping can be found here: [Initial List of Solutions](https://gitlab.com/groups/gaia-x/lab/-/wikis/Initial-List-of-Solutions).

The following considerations apply:

- Does Gaia-X want to join an existing ecosystem incl. governance, restrictions and limits, or does Gaia-X want to create an own identity ecosystem? The answer is most likely: An own ecosystem. Gaia-X wants to create an own Gaia-X registry and there should be no central trust provider or controller of foreign infrastructure components. Particularly no government authorities should be able to control the ecosystem or influence it in a negative sense. [11]
- Does Gaia-X want to create own DIDs, VCs and protocols to enable data exchange between parties in the ecosystem? The answer is partly yes. Own DIDs and VCs are planned, while mature protocols are most likely to be reused.
- Does Gaia-X want to create own applications that enable a good user experience and the desired functionalities? Party yes. Existing solutions should be adapted and reused wherever possible.

## 4. Initial Criteria

Criteria are not ranked yet. The architecture model, considerations of the working groups and others will be taken into account.

Most important decisions are: [3]

**1. Ecosystem & Interoperability** - _Does the solution support the ecosystems (incl. technology framework, specifications, business logic) you selected based on your business and regulatory requirements? What kind of registry is possible? What kind of SSI Flavour is supported?_

- Public-Permissioned / Public-Permissionless
- Supported DID-Methods (Web, Key, EBSI, ETHR, Sovrin, etc.)
- Supported Cryptographic Algorithms (RSA, ED25519, X25519, SECP256K1, etc.)

**2. Open Source vs. Closed Source** - _Is the solution open source? If yes, does it use a permissive
license like MIT or Apache 2?_

- Open Source Code
- License Models (Apache 2.0, MIT, EUPL 1.2, other)

**3. Deployment Options** - _Does the solution support your operational strategy and preferred deployment options (e.g. self-managed on-prem or in cloud vs. managed cloud service)?_

- Available Components (Issuer, Verifier, Wallet, complete SSI-Kit or SDK)
- ...

**4. Integration** - _Is the solution compatible and easily integrated with
existing IT infrastructure?_

- Used Languages and Frameworks (Javascript, Typescript, Rust, Python, Solidity, etc.)
- ...

**5. Available Services** - _Is there an offering of services that will help you navigate the introduction of a new identity infrastructure and mitigate risks for your project’s success? If yes, which services are offered?_

- Available Components (Issuer, Verifier, Wallet, complete SSI-Kit or SDK)
- Estimated Technical Readiness Level (TRL)
- Crendential Exchange Protocols like OIDC/SIOP / DIDComm supported?

##6. Alignment with Architecture**

Gaia-X aims to build a self-sovereign identity ecosystem that does not center around a single identity provider. It is important to keep this in focus during the evaluation of services.

See [15] page 21.

"In the self-sovereign identity (SSI) model, the user is the central administrator of their identity and they have much more control over their data and information than others have, know, or share about them. Unlike centralized, third-party, and federative models, the SSI approach does not require an entity for managing people’s identity. Neither an identity provider nor a service provider is needed to manage one’s credentials and authenticators on their behalf. The role of the identity provider is now limited to an identity issuer. "

![image](uploads/88d608a9e81a6b0801e1d3a41bd9c332/image.png)

Copyright © 2020 Inter-American Development Bank, CC-IGO 3.0 BY-NC-ND 

## 5. Requirements and Considerations

These requirements and considerations are based on SSI comparisons and evaluation frameworks and subject to discussion with the community, such as [LACChain ID Framework](https://publications.iadb.org/publications/english/document/LACChain-ID-Framework-A-Set-of-Recommendations-for-Blockchain-Based-Interoperable-Privacy-Preserving-Regulatory-Compliant-Secure-and-Standardized-Digital-Identifiers-Credentials-and-Wallets.pdf) CC-IGO 3.0 BY-NC-ND Inter-American Development Bank, [walt.id Me, myself and (SS)I](https://drive.google.com/file/d/1D2i4GJQ0ctXWi2Sk_u-ctmmlphaDBvMe/view), [walt.id The Pilot Playbook. Building Pilots with Self-Sovereign Identity.](https://drive.google.com/file/d/1yyiN5bxl7h2rJN9KerTu-1EvyQdUOOkv/view).

### 5.1 DID Documents [1]

#### DID documents MUST be comprised of the following standard elements:
- A Uniform Resource Identifier (URI) to uniquely identify terminology and protocols that allow parties to read the DID document.
- A DID that identifies the subject of the DID document
- A set of authenticators (i.e., public keys) used for authentication, authorization, and communication mechanisms
- A set of authentication methods used for the DID subject to prove ownership of the DID to another entity
- A set of service endpoints to describe where and how to interact with the DID subject
- A timestamp for when the document was created
- A timestamp for when the document was last updated
- Cryptographic proof of identity (e.g., digital signature)

#### DID documents SHOULD:
- include a set of authorization and delegation methods for allowing other entities to operate on behalf of the DID subject (i.e., holders different from the subject)
- Contain an element that indicates the DID status (active, suspended, or revoked)
- Contains attributes within the W3C standard DID definition

#### DID documents COULD:
- be stored in a distributed ledger, which allows creation of public and decentralized DID registries so that DIDs can easily be resolved against an accessible, trusted, and decentralized ledger

#### DID documents MUST NOT:
- include PII, where in violation with GDPR. 

### 5.2 DID Methods [1]

Manifestations of the DID standard are referred to as DID methods:

#### DID Methods MUST:
- Have more than one authentication method (i.e., RSA, EC)
- Not disclose any personal data or information in the DID documents, privacy by design
- Guarantee privacy and pseudonymity in the use of the DIDs
- use of DID Documents in JSON format for document representation
- Be scalable enough to economically afford the generation of the required amount of DIDs for the specific use case in the chosen network

#### DID Methods SHOULD:

- Register the DIDs in a smart contract with a well-defined governance (an on-chain DID registry) so that issuers or verifiers that intend to resolve a DID can easily find it in a public ledger

#### DID Methods COULD:
- Set different functionalities for the different keys, so that some primary keys can be used for authentication, some secondary keys can be used for temporary delegation, and some tertiary keys can be used for retrieving primary and secondary keys
- Use quantum-safe cryptography for authentication, encryption, and signature, and include a variety of cryptographic algorithms
- Allow -responsible- use of biometrics (by wallets and applications used by individuals to manage their digital identity information)

#### DID Methods MUST NOT:
- rely on one central party for storage and verification

### 5.3 Wallets [1]

Wallets are custodial or non-custodial repositories that allow owners to store, manage, and present both keys and identity credentials. Within the B2B context of Gaia-X custodial and non-custodial wallets are accepted.

There are many different wallet types available:
- Desktop wallets (installed onto a particular computer)
- Browser wallets (browser extensions installed in a particular computer)
- Hardware wallets (physical devices such as a hard drive or USB)
- Cloud wallets (based in cloud-storage)
- Mobile wallets (mobile applications)

For the initial assessment the Lab focusses on cloud wallets, mobile wallets and desktop/browser wallets. Hardware wallets meet the highest security requirements, but are the latest in the development order.

#### Digital wallets MUST comply with the following requirements: 

- Provide secure access to the holder, by guaranteeing that only authorized entities can access it. The wallet requires a minimum set of authenticator factors to the user.
- Ensure security and strong data encryption.
- Do not have access to subject’s private keys.
- Be connected to the ledgers where the DID registries, trusted lists, and cryptographic proofs of the DID documents and credentials are stored.
- comply with data privacy and data protection regulations.

#### Digital wallets SHOULD comply with the following requirements:
- Provide mechanisms for reducing PII of the entities’ activities by combining the use of different DIDs for different interactions.
- Become certified and/or audited to be acknowledged as trusted services.
- Keep transactional information about the subjects, if authorized to it.
- Provide recovery of keys and credentials.

#### Digital wallets COULD comply with the following requirements:
- Provide mechanisms for subjects and issuers to change the status of their credentials.
- Provide mechanisms for the owner to erase all the data associated with them
(including revoking DIDs and VCs).
- Provide dashboards of activity.

#### Digital wallets MUST NOT comply with the following requirements:
- tbd

## I Acronyms [1]

| Acronym | Explanation |
| ------ | ------ |
| **DID** | Decentralized Identifier |
| **DID Connect** | extension of OIDC |
| **DIF** | Decentralized Identity Foundation |
| **DDO** | DID DOcument  |
| **DLT**  | Distributed Ledger Technology  |
| **eIDAS** |Electronic IDentification, Authentication and Trust Services |
| **EBSI** | European Blockchain Services Infrastructure |
| **ECDSA** | Elliptic Curve Digital Signature Algorithm |
| **ESSIF** | European Self-Sovereign Identity Framework |
| **GDPR** | General Data Protection Regulation |
| **IPFS** | InterPlanetary File System |
| **JWS** | JSON Web Signature |
| **OIDC** | OpenID Connect |
| **OIDC4VP** | OpenID Connect for Verifiable Presentations |
| **OP** | OpenID Provider |
| **PII** | Personally Identifiable Information |
| **PKD** | Public Key Directory |
| **PKI** | Public Key Infrastructure |
| **RP** | Relying Party in OIDC |
| **RSA** | Rivest–Shamir–Adleman is a public-key algorithm |
| **SHA-256** | Secure Hash Algorithm 256 bits |
| **SSL** | Secure Shell Protocol |
| **SSI** | Self-Sovereign Identity |
| **URI** | Uniform Resource Identifier |
| **UUID** | Universally Unique Identifier |
| **VC** | Verifiable Credential |
| **W3C** | World Wide Web Consortium |
| **ZKP** | Zero-knowledge Proof |

## II Resources

[0] [Gaia-X Architecture Document 21.12.](https://gaia-x.eu/sites/default/files/2022-01/Gaia-X_Architecture_Document_2112.pdf)

[1] [LACCHAIN ID FRAMEWORK, © 2021 Inter-American Development Bank](https://publications.iadb.org/en/lacchain-id-framework-set-recommendations-blockchain-based-interoperable-privacy-preserving)

[2] [walt.id Me, myself and (SS)I](https://drive.google.com/file/d/1D2i4GJQ0ctXWi2Sk_u-ctmmlphaDBvMe/view)

[3] [walt.id The Pilot Playbook. Building Pilots with Self-Sovereign Identity.](https://drive.google.com/file/d/1yyiN5bxl7h2rJN9KerTu-1EvyQdUOOkv/view)

[4] [The Path to Self-Sovereign Identity by Christopher Allen](http://www.lifewithalacrity.com/2016/04/the-path-to-self-soverereign-identity.html)

[5] [eIDAS supported Self-Sovereign Identity](https://ec.europa.eu/futurium/en/system/files/ged/eidas_supported_ssi_may_2019_0.pdf)

[6] W3C [Verifiable Credentials Data Model v1.1](https://www.w3.org/TR/vc-data-model/)

[7] W3C [Decentralized Identifier Resolution (DID Resolution) v0.2](https://w3c-ccg.github.io/did-resolution/)

[8] GXFS [Gaia-X Personal Credential Manager](https://www.gxfs.de/wp-content/uploads/2021/06/SRS_GXFS_IDM_PCM.pdf)

[9] GXFS [Gaia-X Organization Credential Manager](https://www.gxfs.de/wp-content/uploads/2021/06/SRS_GXFS_IDM_OCM.pdf)

[10] GXFS [Gaia-X Notarization API](https://www.gxfs.de/wp-content/uploads/2021/06/SRS_GXFS_CP_NOTAR.pdf)

[11] GXFS [Reference Document Identity and Access Management](https://www.gxfs.de/wp-content/uploads/2021/08/annex_GX_IDM_AO.pdf)

[12] [Identity Management with Blockchain: The Definitive Guide](https://tykn.tech/identity-management-blockchain/)

[13] Phil Windley [on Twitter](https://twitter.com/windley/status/1071469217650638848?s=20&t=6dH-UDrG0Ujsgu_0UzQxVA)

[14] [JSON Web Signature 2020](https://w3c-ccg.github.io/lds-jws2020/)

[15] [Self-Sovereign Identity: The Future of Identity: Self-Sovereignity, Digital Wallets, and Blockchain](https://publications.iadb.org/en/self-sovereign-identity-future-identity-self-sovereignity-digital-wallets-and-blockchain)

[16] SIOPv2 [Self-Issued OpenID Provider v2](https://openid.net/specs/openid-connect-self-issued-v2-1_0.html)

[17] OIDC4VP [OpenID Connect for Verifiable Presentations](https://openid.net/specs/openid-connect-4-verifiable-presentations-1_0.html)

[18] OIDC Core [OpenID Connect Core 1.0](https://openid.net/specs/openid-connect-core-1_0.html)

[19] [GaiaX Community Document IAM](https://www.gxfs.de/wp-content/uploads/2021/08/annex_GX_IDM_AO.pdf)

[20] [GXFS Authentication/ Authorization](https://www.gxfs.de/wp-content/uploads/2021/06/SRS_GXFS_IDM_AA.pdf)

[21] [GXFS Trust Services API](https://www.gxfs.de/wp-content/uploads/2021/06/SRS_GXFS_IDM_TSA.pdf)

[22] [DIDComm Messaging](https://identity.foundation/didcomm-messaging/spec/)